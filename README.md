# RentManager

ASI311 - 2019

Baptiste COSTE  
Sébastien CHAUVEAU  

## Application

Application de gestion de location de véhicules

## Fonctionnalités implémentées

### Contraintes
- Un champ de type “email” ne peut être valide que s’il contient un “@” et une extension “.com” ou “.fr”.
- Un utilisateur ne peut être ajouté que s’il est majeur.
- S’il est saisi, la date du champ début de Réservation doit de situer avant la date fin.


### Véhicules

- Afficher la liste des véhicules présents dans la base de données
- Insérer un véhicule dans la base de données
- Supprimer un véhicule (erreur rencontrée : il est parfois impossible de supprimer un certain véhicule)

### Page d’accueil

- Afficher le nombre de véhicules présents dans la base de données
- Afficher le nombre d’utilisateurs présents dans la base de données

### messages d'erreur

- Sur toutes ces pages, afficher un message d’erreur si quelque chose ne se passe pas comme prévu (exemple : validation des données, envoi d’une requête à la base de données)
Pour ce faire, nous avons créé une page jsp qui nous permet d'afficher le message d'erreur que l'on veut. Pour chaque controller (home, client, vehicle), lorsqu'une exception est attrapée par une méthode du controller, elle envoie une requête GET à l'url : /[nom du controller]/error qui est gérée par le controller en question à travers la fonction showError() qui affiche le contenu de error.jsp en modifiant la page en fonction du message d'erreur qui est passé en paramètres dans la requête GET correspondante.

### Clients

- Afficher la liste des clients présents dans la base de données
- Insérer un client dans la base de données. La date de naissance de ce dernier doit être saisie sous forme de String (ex : 10/01/2019) et être convertie en LocalDate. 
- Supprimer un client

## TODO

### Réservations

- Afficher la liste des réservations présentes dans la base de données
- Insérer une réservation dans la base de données
    - Afficher la liste des véhicules dans une liste déroulante afin que l’utilisateur puisse la sélectionner facilement
    - Afficher la liste des clients dans une liste déroulante afin que l’utilisateur puisse le sélectionner facilement
- Supprimer une réservation



