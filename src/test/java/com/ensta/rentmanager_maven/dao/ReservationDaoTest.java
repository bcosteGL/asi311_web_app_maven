package com.ensta.rentmanager_maven.dao;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.sql.Connection;

import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertFalse;
import org.junit.jupiter.api.BeforeEach;
import static org.mockito.Mockito.mock;

import com.ensta.rentmanager_maven.exception.DaoException;

public class ReservationDaoTest {
	private Connection connection;
	@BeforeEach
	void init() {
		this.connection = mock(Connection.class);
	}
	@Test
	void findAll_return_should_not_be_null() throws DaoException
	{
		ReservationDao reservationDao = new ReservationDao(this.connection);
		assertTrue((((reservationDao).findAll()) != null));
	}
	
	@Test
	void findAll_should_not_throw_exception()
	{
		ReservationDao reservationDao = new ReservationDao(this.connection);
		boolean answer = true;
		try {
			reservationDao.findAll();
		} catch (DaoException e) {
			answer = false;
		}
		assertTrue(answer);
	}
	
	@Test
	void findResaById_return_should_not_be_empty() throws DaoException
	{
		ReservationDao reservationDao = new ReservationDao(this.connection);
		assertTrue((((reservationDao).findResaById(1)) != null));
	}
	void findResaById_return_should_be_empty() throws DaoException
	{
		ReservationDao reservationDao = new ReservationDao(this.connection);
		assertFalse((((reservationDao).findResaById(-5) != null)));
	}
	
	void findResaById_should_not_throw_exception()
	{
		ReservationDao reservationDao = new ReservationDao(this.connection);
		boolean answer = true;
		try {
			reservationDao.findResaById(1);
		} catch (DaoException e) {
			answer = false;
		}
		assertTrue(answer);
	}
}