package com.ensta.rentmanager_maven.dao;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertFalse;

import org.hibernate.Session;
import org.junit.jupiter.api.BeforeEach;
import static org.mockito.Mockito.mock;

import com.ensta.rentmanager_maven.exception.DaoException;

public class VehicleDaoTest {
	private Session session;
	@BeforeEach
	void init() {
		this.session = mock(Session.class);
	}
	@Test
	void findAll_return_should_not_be_null() throws DaoException
	{
		VehicleDao vehicleDao = new VehicleDao(this.session);
		assertTrue((((vehicleDao).findAll()) != null));
	}
	
	@Test
	void findAll_should_not_throw_exception()
	{
		VehicleDao vehicleDao = new VehicleDao(this.session);
		boolean answer = true;
		try {
			vehicleDao.findAll();
		} catch (DaoException e) {
			answer = false;
		}
		assertTrue(answer);
	}
	
	@Test
	void findById_return_should_not_be_empty() throws DaoException
	{
		VehicleDao vehicleDao = new VehicleDao(this.session);
		assertTrue((((vehicleDao).findById(1)).isPresent()));
	}
	void findById_return_should_be_empty() throws DaoException
	{
		VehicleDao vehicleDao = new VehicleDao(this.session);
		assertFalse((((vehicleDao).findById(-5)).isPresent()));
	}
	
	void findById_should_not_throw_exception()
	{
		VehicleDao vehicleDao = new VehicleDao(this.session);
		boolean answer = true;
		try {
			vehicleDao.findById(2);
		} catch (DaoException e) {
			answer = false;
		}
		assertTrue(answer);
	}
}