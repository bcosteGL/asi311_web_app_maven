package com.ensta.rentmanager_maven.dao;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import org.hibernate.Session;
import static org.junit.Assert.assertFalse;
import org.junit.jupiter.api.BeforeEach;
import static org.mockito.Mockito.mock;


import com.ensta.rentmanager_maven.exception.DaoException;

public class ClientDaoTest {
	private Session session;
	@BeforeEach
	void init() {
		this.session = mock(Session.class);
	}
	@Test
	void findAll_return_should_not_be_null() throws DaoException
	{
		ClientDao clientDao = new ClientDao(this.session);
		assertTrue((((clientDao).findAll()) != null));
	}
	
	@Test
	void findAll_should_not_throw_exception()
	{
		ClientDao clientDao = new ClientDao(this.session);
		boolean answer = true;
		try {
			clientDao.findAll();
		} catch (DaoException e) {
			answer = false;
		}
		assertTrue(answer);
	}
	
	@Test
	void findById_return_should_not_be_empty() throws DaoException
	{
		ClientDao clientDao = new ClientDao(this.session);
		assertTrue((((clientDao).findById(1)).isPresent()));
	}
	void findById_return_should_be_empty() throws DaoException
	{
		ClientDao clientDao = new ClientDao(this.session);
		assertFalse((((clientDao).findById(-5)).isPresent()));
	}
	
	void findById_should_not_throw_exception()
	{
		ClientDao clientDao = new ClientDao(this.session);
		boolean answer = true;
		try {
			clientDao.findById(2);
		} catch (DaoException e) {
			answer = false;
		}
		assertTrue(answer);
	}
}