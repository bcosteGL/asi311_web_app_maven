package com.ensta.rentmanager_maven.service;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.ensta.rentmanager_maven.dao.VehicleDao;
import com.ensta.rentmanager_maven.exception.ServiceException;


public class VehicleServiceTest {
	private VehicleDao vehicleDao;
	@BeforeEach
	void init() {
		this.vehicleDao = mock(VehicleDao.class);
	}
	@Test
	void findById_return_should_not_be_empty() throws ServiceException
	{
		VehicleService vehicleService = new VehicleService(this.vehicleDao);
		assertTrue((((vehicleService).findById(1)).isPresent()));
	}
	
	@Test
	void findById_return_should_be_empty() throws ServiceException
	{
		VehicleService vehicleService = new VehicleService(this.vehicleDao);
		assertFalse((((vehicleService).findById(-1)).isPresent()));
	}
	
	@Test
	void findById_should_not_throw_exception()
	{
		VehicleService vehicleService = new VehicleService(this.vehicleDao);
		boolean answer = true;
		try {
			vehicleService.findById(1);
		} catch (ServiceException e) {
			answer = false;
		}
		assertTrue(answer);
	}
}