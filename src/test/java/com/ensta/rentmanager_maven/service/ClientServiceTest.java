package com.ensta.rentmanager_maven.service;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.ensta.rentmanager_maven.dao.ClientDao;
import com.ensta.rentmanager_maven.exception.ServiceException;


public class ClientServiceTest {
	private ClientDao clientDao;
	@BeforeEach
	void init() {
		this.clientDao = mock(ClientDao.class);
	}
	@Test
	void findById_return_should_not_be_empty() throws ServiceException
	{
		ClientService clientService = new ClientService(this.clientDao);
		assertTrue((((clientService).findById(1)).isPresent()));
	}
	
	@Test
	void findById_return_should_be_empty() throws ServiceException
	{
		ClientService clientService = new ClientService(this.clientDao);
		assertFalse((((clientService).findById(-1)).isPresent()));
	}
	
	@Test
	void findById_should_not_throw_exception()
	{
		ClientService clientService = new ClientService(this.clientDao);
		boolean answer = true;
		try {
			clientService.findById(1);
		} catch (ServiceException e) {
			answer = false;
		}
		assertTrue(answer);
	}
}