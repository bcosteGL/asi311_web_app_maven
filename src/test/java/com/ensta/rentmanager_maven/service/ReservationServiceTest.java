package com.ensta.rentmanager_maven.service;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.ensta.rentmanager_maven.dao.ReservationDao;
import com.ensta.rentmanager_maven.exception.ServiceException;

public class ReservationServiceTest {
	private ReservationDao reservationDao;
	@BeforeEach
	void init() {
		this.reservationDao = mock(ReservationDao.class);
	}
	@Test
	void findById_return_should_not_be_empty() throws ServiceException
	{
		ReservationService reservationService = new ReservationService(this.reservationDao);
		assertTrue((((reservationService).findById(1)) != null));
	}
	
	@Test
	void findById_return_should_be_empty() throws ServiceException
	{
		ReservationService reservationService = new ReservationService(this.reservationDao);
		assertFalse((((reservationService).findById(-1)) != null));
	}
	
	@Test
	void findById_should_not_throw_exception()
	{
		ReservationService reservationService = new ReservationService(this.reservationDao);
		boolean answer = true;
		try {
			reservationService.findById(1);
		} catch (ServiceException e) {
			answer = false;
		}
		assertTrue(answer);
	}
}