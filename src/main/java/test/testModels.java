package test;
import java.time.LocalDate;

import com.ensta.rentmanager_maven.model.*;

public class testModels {

	public static void main(String[] args)
	{
		Client client = new Client(8,"Bon","Jean", "jeanbon@zeb.com", LocalDate.now());
		System.out.print(client.toString());
		
		Reservation reservation = new Reservation(7, 5, 2, LocalDate.now(), LocalDate.now());
		System.out.print(reservation.toString());
		
		Vehicle vehicle = new Vehicle(7, "Ford", "Clio", 4);
		System.out.print(vehicle.toString());
	}
}
