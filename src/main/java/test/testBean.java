package test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


import com.ensta.rentmanager_maven.configuration.AppConfiguration;
import com.ensta.rentmanager_maven.dao.ClientDao;
import com.ensta.rentmanager_maven.exception.DaoException;
import com.ensta.rentmanager_maven.exception.ServiceException;
import com.ensta.rentmanager_maven.persistence.ConnectionManager;
import com.ensta.rentmanager_maven.service.ClientService;

public class testBean {
	public static void main(String[] args) {
		/** Q1
		 * ApplicationContext context = new AnnotationConfigApplicationContext(AppConfiguration.class);
		ConnectionManager bean = context.getBean(ConnectionManager.class);
		System.out.println(bean.test()); **/
		
		ApplicationContext context = new AnnotationConfigApplicationContext(AppConfiguration.class);
		ClientService clientService = context.getBean(ClientService.class);
		try {
			System.out.println(clientService.findAll() != null);
		} catch (ServiceException e) {
		}
		
	}
}
