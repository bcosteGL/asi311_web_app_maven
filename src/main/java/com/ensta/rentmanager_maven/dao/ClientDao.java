package com.ensta.rentmanager_maven.dao;


import java.util.List;
import java.util.Optional;

import javax.persistence.PersistenceException;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ensta.rentmanager_maven.exception.DaoException;
import com.ensta.rentmanager_maven.model.Client;

@Repository
public class ClientDao {
	
	private final Session session;
	
	@Autowired
	public ClientDao(Session session) {
		this.session = session;
	}
	
	private static final String FIND_ALL_QUERY = "SELECT v FROM Client v";
	private static final String COUNT_QUERY = "SELECT COUNT(id) AS count FROM Client v";
	
	public long create(Client client) throws DaoException {
		try {
			session.save(client);
			return client.getId();
		} catch (PersistenceException e) {
			throw new DaoException();
		}
	}
	
	public void delete(Client client) throws DaoException {
		try {
			session.beginTransaction();
			session.delete(client);
			session.getTransaction().commit();
		} catch (PersistenceException e) {
			throw new DaoException();
		}
	}
	
	public Optional<Client> findById(long id) throws DaoException {
		try {
		Client client = session.find(Client.class, ((Long)id).intValue());
			return Optional.ofNullable(client);
		} catch (PersistenceException e) {
			throw new DaoException();
		}
	}


	public List<Client> findAll() throws DaoException {
		try {
			Query query = session.createQuery(FIND_ALL_QUERY);
			return (List<Client>) query.getResultList();
		} catch (PersistenceException e) {
			throw new DaoException();
		}
	}
	
	public int count() throws DaoException {
		try {
			Query query = session.createQuery(COUNT_QUERY);
			return ((Long)(query.uniqueResult())).intValue();
		} catch (PersistenceException e) {
			throw new DaoException();
		}
	}		

}

