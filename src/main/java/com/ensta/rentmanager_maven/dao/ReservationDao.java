package com.ensta.rentmanager_maven.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ensta.rentmanager_maven.exception.DaoException;
import com.ensta.rentmanager_maven.model.Reservation;

@Repository
public class ReservationDao {


	private Connection connection;
	
	@Autowired
	public ReservationDao(Connection connection) {
		this.connection = connection;
	}
    
    private static final String CREATE_QUERY = "INSERT INTO Reservation(client_id, vehicule_id, debut, fin) VALUES (?, ?, ?, ?);";
    private static final String DELETE_QUERY = "DELETE FROM Reservation WHERE id = ?;";
    private static final String FIND_ALL_QUERY = "SELECT id, client_id, vehicule_id, debut, fin FROM Reservation;";
    private static final String FIND_CLI_QUERY = "SELECT id, client_id, vehicule_id, debut, fin FROM Reservation WHERE client_id = ?;";
    private static final String FIND_VEH_QUERY = "SELECT id, client_id, vehicule_id, debut, fin FROM Reservation WHERE vehicule_id = ?;";
    private static final String FIND_QUERY = "SELECT id, client_id, vehicule_id, debut, fin FROM Reservation WHERE id = ?;";

	public long create(Reservation reservation) throws DaoException {
		ResultSet res = null;
		PreparedStatement preparedStatement = null;
		int id = -1;
		try {
			preparedStatement = connection.prepareStatement(CREATE_QUERY, Statement.RETURN_GENERATED_KEYS);
			
			preparedStatement.setInt(2, reservation.getVehicleId());
			preparedStatement.setInt(1, reservation.getClientId());
			preparedStatement.setDate(3, Date.valueOf(reservation.getStartDate()));
			preparedStatement.setDate(4, Date.valueOf(reservation.getEndDate()));
			
			preparedStatement.executeUpdate();
			res = preparedStatement.getGeneratedKeys();
			
			if (res.next()) {
				id = res.getInt(1);
			}
			
		} catch (SQLException e) {
			throw new DaoException("Error DAO Create Reservation");
		}
		
		try {
			res.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		try {
			preparedStatement.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return(id);
	}
	
	public long delete(Reservation reservation) throws DaoException {
		PreparedStatement preparedStatement = null;
		if (reservation == null) {
			return 1;
		}
		try {
			preparedStatement = connection.prepareStatement(DELETE_QUERY);
			
			preparedStatement.setInt(1, reservation.getId());
			
			preparedStatement.executeUpdate();
			
		} catch (SQLException e) {
			throw new DaoException("Error DAO Delete Reservation");
		}
		
		try {
			preparedStatement.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	public Reservation findResaById(long Id) throws DaoException {
		ResultSet res = null;
		PreparedStatement preparedStatement = null;
		Reservation reservation = new Reservation();
		
		try {
			preparedStatement = connection.prepareStatement(FIND_QUERY);
			preparedStatement.setLong(1, Id);
			res = preparedStatement.executeQuery();
			
			while (res.next()) {
				reservation.setId(res.getInt("id"));
				reservation.setClientId(res.getInt("client_id"));
				reservation.setVehicleId(res.getInt("vehicule_id"));
				reservation.setStartDate(res.getDate("debut").toLocalDate());
				reservation.setEndDate(res.getDate("fin").toLocalDate());
			}
			
		} catch (SQLException e) {
			throw new DaoException("Error DAO Find Reservation");
		}
		
		try {
			res.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		try {
			preparedStatement.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
		return reservation;
	}
	
	public List<Reservation> findResaByClientId(long clientId) throws DaoException {
		ResultSet res = null;
		PreparedStatement preparedStatement = null;
		List<Reservation> listReservation = new ArrayList<Reservation>();
		
		try {
			preparedStatement = connection.prepareStatement(FIND_CLI_QUERY);
			preparedStatement.setLong(1, clientId);
			res = preparedStatement.executeQuery();
			
			while (res.next()) {
				Reservation reservation = new Reservation();
				reservation.setId(res.getInt("id"));
				reservation.setVehicleId(res.getInt("vehicule_id"));
				reservation.setClientId(res.getInt("client_id"));
				
				reservation.setStartDate(res.getDate("debut").toLocalDate());
				reservation.setEndDate(res.getDate("fin").toLocalDate());
				listReservation.add(reservation);
			}
			
		} catch (SQLException e) {
			throw new DaoException("Error DAO Find Cli Reservation");
		}
		
		try {
			res.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		try {
			preparedStatement.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
		return listReservation;
	}
	
	public List<Reservation> findResaByVehicleId(long vehicleId) throws DaoException {
		ResultSet res = null;
		PreparedStatement preparedStatement = null;
		List<Reservation> listReservation = new ArrayList<Reservation>();
		
		try {
			preparedStatement = connection.prepareStatement(FIND_VEH_QUERY);
			preparedStatement.setLong(1, vehicleId);
			res = preparedStatement.executeQuery();
			
			while (res.next()) {
				Reservation reservation = new Reservation();
				reservation.setId(res.getInt("id"));
				reservation.setClientId(res.getInt("client_id"));
				reservation.setVehicleId(res.getInt("vehicule_id"));
				reservation.setStartDate(res.getDate("debut").toLocalDate());
				reservation.setEndDate(res.getDate("fin").toLocalDate());
				listReservation.add(reservation);
			}
			
		} catch (SQLException e) {
			throw new DaoException("Error DAO Find Veh Reservation");
		}
		
		try {
			res.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		try {
			preparedStatement.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
		return listReservation;
	}

	public List<Reservation> findAll() throws DaoException {
		ResultSet res = null;
		PreparedStatement preparedStatement = null;
		List<Reservation> listReservation = new ArrayList<Reservation>();
		
		try {
			preparedStatement = connection.prepareStatement(FIND_ALL_QUERY);
			res = preparedStatement.executeQuery();
			
			while (res.next()) {
				Reservation reservation = new Reservation();
				reservation.setId(res.getInt("id"));
				reservation.setClientId(res.getInt("client_id"));
				reservation.setVehicleId(res.getInt("vehicule_id"));
				reservation.setStartDate(res.getDate("debut").toLocalDate());
				reservation.setEndDate(res.getDate("fin").toLocalDate());
				listReservation.add(reservation);
			}
			
		} catch (SQLException e) {
			throw new DaoException("Error DAO Find All Reservation");
		}
		
		try {
			res.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		try {
			preparedStatement.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return listReservation;
	}
}
