package com.ensta.rentmanager_maven.dao;


import java.util.List;
import java.util.Optional;

import javax.persistence.PersistenceException;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ensta.rentmanager_maven.exception.DaoException;
import com.ensta.rentmanager_maven.model.Vehicle;

@Repository
public class VehicleDao {
	
	private final Session session;
	
	@Autowired
	public VehicleDao(Session session) {
		this.session = session;
	}
	
	private static final String FIND_ALL_QUERY = "SELECT v FROM Vehicle v";
	private static final String COUNT_QUERY = "SELECT COUNT(id) AS count FROM Vehicle v";
	
	public long create(Vehicle vehicle) throws DaoException {
		try {
			session.save(vehicle);
			return vehicle.getId();
		} catch (PersistenceException e) {
			throw new DaoException();
		}
	}
	
	public void delete(Vehicle vehicle) throws DaoException {
		try {
			session.beginTransaction();
			session.delete(vehicle);
			session.getTransaction().commit();
		} catch (PersistenceException e) {
			throw new DaoException();
		}
	}
	
	public Optional<Vehicle> findById(long id) throws DaoException {
		try {
		Vehicle vehicle = session.find(Vehicle.class, ((Long)id).intValue());
			return Optional.ofNullable(vehicle);
		} catch (PersistenceException e) {
			throw new DaoException();
		}
	}


	public List<Vehicle> findAll() throws DaoException {
		try {
			Query query = session.createQuery(FIND_ALL_QUERY);
			return (List<Vehicle>) query.getResultList();
		} catch (PersistenceException e) {
			throw new DaoException();
		}
	}
	
	public int count() throws DaoException {
		try {
			Query query = session.createQuery(COUNT_QUERY);
			return ((Long)(query.uniqueResult())).intValue();
		} catch (PersistenceException e) {
			throw new DaoException();
		}
	}		

}

