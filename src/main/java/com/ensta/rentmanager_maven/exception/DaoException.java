package com.ensta.rentmanager_maven.exception;

public class DaoException extends Exception{
	
	private static final long serialVersionUID = 1L;

	public DaoException()
	{
		super();
	}
	
	public DaoException(String message)
	{
		super(message);
	}
}
