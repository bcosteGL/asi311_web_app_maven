package com.ensta.rentmanager_maven.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Client")
public class Client {
	
	//Attributs
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name = "nom")
	private String name;
	@Column(name = "prenom")
	private String firstName;
	@Column(name = "email")
	private String email;
	@Column(name = "naissance")
	private LocalDate birthDate;
	
	//Constructeurs
	
	public Client()
	{
		super();
	}
	
	public Client(String name, String firstName, String email, LocalDate birthDate)
	{
		this();
		this.name = name;
		this.firstName = firstName;
		this.email = email;
		this.birthDate = birthDate;
	}
	
	public Client(int id, String name, String firstName, String email, LocalDate birthDate)
	{
		this();
		this.id = id;
		this.name = name;
		this.firstName = firstName;
		this.email = email;
		this.birthDate = birthDate;
	}
	
	//Getter
	
	public int getId()
	{
		return(this.id);
	}
	public String getName()
	{
		return(this.name);
	}
	public String getFirstName()
	{
		return(this.firstName);
	}
	public String getEmail()
	{
		return(this.email);
	}
	public LocalDate getBirthDate()
	{
		return(this.birthDate);
	}
	
	//Setter
	
	public void setId(int id)
	{
		this.id = id;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}
	public void setEmail(String email)
	{
		this.email = email;
	}
	public void setBirthDate(LocalDate birthDate)
	{
		this.birthDate = birthDate;
	}
	
	// Parse
	
	public String toString()
	{
		return(this.firstName + " " + this.name + ", client numéro " + String.valueOf(this.id) + " né(e) le " + this.birthDate.toString() + ", email : " + this.email + "\n");
	}
}
