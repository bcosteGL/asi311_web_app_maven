package com.ensta.rentmanager_maven.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Vehicule")
public class Vehicle {
	
	//Attributs
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name = "constructeur")
	private String constructor;
	@Column(name = "modele")
	private String model;
	@Column(name = "nb_places")
	private int size;

	//Constructors
	
	public Vehicle()
	{
		super();
	}
	public Vehicle(String constructor, String model, int size)
	{
		this();
		this.constructor = constructor;
		this.model = model;
		this.size = size;
	}
	public Vehicle(int id, String constructor, String model, int size)
	{
		this();
		this.id = id;
		this.constructor = constructor;
		this.model = model;
		this.size = size;
	}
	
	//Getter
	
	public int getId()
	{
		return(this.id);
	}
	public String getConstructor()
	{
		return(this.constructor);
	}
	public String getModel()
	{
		return(this.model);
	}
	public int getSize()
	{
		return(this.size);
	}
	
	//Setter
	
	public void setId(int id)
	{
		this.id = id;
	}
	public void setConstructor(String constructor)
	{
		this.constructor = constructor;
	}
	public void setModel(String model)
	{
		this.model = model;
	}
	public void setSize(int size)
	{
		this.size = size;
	}
	
	// Parse
	
	public String toString()
	{
		return("Véhicule numéro " + String.valueOf(this.id) + ", construit par " + this.constructor + ", modèle " + this.model + " avec " + String.valueOf(this.size) + " place(s)\n");
	}
}
