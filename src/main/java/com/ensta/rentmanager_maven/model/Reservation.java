package com.ensta.rentmanager_maven.model;

import java.time.LocalDate;

public class Reservation {

	//Attributs
	
	private int id;
	private int clientId;
	private int vehicleId;
	private LocalDate startDate;
	private LocalDate endDate;

	//Constructeurs
	
	public Reservation()
	{
		super();
	}
	public Reservation(int clientId, int vehicleId, LocalDate startDate, LocalDate endDate)
	{
		this();
		this.clientId = clientId;
		this.vehicleId = vehicleId;
		this.startDate = startDate;
		this.endDate = endDate;
	}
	public Reservation(int id, int clientId, int vehicleId, LocalDate startDate, LocalDate endDate)
	{
		this();
		this.id = id;
		this.clientId = clientId;
		this.vehicleId = vehicleId;
		this.startDate = startDate;
		this.endDate = endDate;
	}
	
	//Getter
	
	public int getId()
	{
		return(this.id);
	}
	public int getClientId()
	{
		return(this.clientId);
	}
	public int getVehicleId()
	{
		return(this.vehicleId);
	}
	public LocalDate getStartDate()
	{
		return(this.startDate);
	}
	public LocalDate getEndDate()
	{
		return(this.endDate);
	}
	
	//Setter
	
	public void setId(int id)
	{
		this.id = id;
	}
	public void setClientId(int clientId)
	{
		this.clientId = clientId;
	}
	public void setVehicleId(int vehicleId)
	{
		this.vehicleId = vehicleId;
	}
	public void setStartDate(LocalDate startDate)
	{
		this.startDate = startDate;
	}
	public void setEndDate(LocalDate endDate)
	{
		this.endDate = endDate;;
	}
	
	// Parse
	
	public String toString()
	{
		return("Réservation numéro " + String.valueOf(this.id) + " du véhicule numéro " + String.valueOf(this.vehicleId) + " du client numéro " + String.valueOf(this.clientId) + " du " + this.startDate.toString() + " au " + this.endDate.toString() + "\n");
	}

	
}
