package com.ensta.rentmanager_maven.controller;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.ensta.rentmanager_maven.exception.ServiceException;
import com.ensta.rentmanager_maven.model.Client;
import com.ensta.rentmanager_maven.service.ClientService;

@Controller
@RequestMapping("/users")
public class ClientController {
	
	@Autowired
	private ClientService clientService;
	
	
	@GetMapping
	public String getAll(Model model, RedirectAttributes redirectAttributes) {
		try {
			List<Client> users = clientService.findAll();
			model.addAttribute("listClients", users);
			
		} catch (ServiceException e) {
			redirectAttributes.addAttribute("errorMessage", e.getMessage());
			return "redirect:/users/error";
		}
		return "/users/list";
	}
	
	@GetMapping("/create")
	public String create(Model model)
	{
		
		return "/users/create";
	}
	
	@PostMapping("/create")
	public String post(@ModelAttribute("client") Client client, RedirectAttributes redirectAttributes)
	{
		try	{
			clientService.create(client);
		} catch (ServiceException e) {
			redirectAttributes.addAttribute("errorMessage", e.getMessage());
			return "redirect:/users/error";
		}		
		return "redirect:/users";
	}
	
	@PostMapping("/delete")
	public String delete(Model model, int id, RedirectAttributes redirectAttributes)
	{
		try {
			clientService.delete(id);
		} catch (ServiceException e) {
			redirectAttributes.addAttribute("errorMessage", e.getMessage());
			return "redirect:/users/error";
		}			
		return "redirect:/users";
	}
	
	@GetMapping("/error")
	public String showError(Model model, String errorMessage)
	{
		model.addAttribute("errorMessage", errorMessage);
		return "/error";
	}

}
