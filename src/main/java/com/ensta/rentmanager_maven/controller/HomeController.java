package com.ensta.rentmanager_maven.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.ensta.rentmanager_maven.exception.ServiceException;
import com.ensta.rentmanager_maven.service.ClientService;
import com.ensta.rentmanager_maven.service.ReservationService;
import com.ensta.rentmanager_maven.service.VehicleService;

@Controller
@RequestMapping("/home")
public class HomeController {
	
	@Autowired
	private VehicleService vehicleService;
	@Autowired
	private ClientService clientService;
	//private ReservationService reservationService;
	
	@GetMapping
	public String get(Model model, RedirectAttributes redirectAttributes) {
		try{
			model.addAttribute("vehicleCount", vehicleService.count());
			model.addAttribute("clientCount", clientService.count());
			//model.addAttribute("reservationCount", reservationService.count());
		} catch (ServiceException e) {
			e.printStackTrace();
			redirectAttributes.addAttribute("errorMessage", e.getMessage());
			return "redirect:/home/error";
		}
		return "home";
	}
	
	@GetMapping("/error")
	public String showError(Model model, String errorMessage)
	{
		model.addAttribute("errorMessage", errorMessage);
		return "/error";
	}
	
}
