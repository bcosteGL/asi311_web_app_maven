package com.ensta.rentmanager_maven.controller;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.ensta.rentmanager_maven.exception.ServiceException;
import com.ensta.rentmanager_maven.model.Vehicle;
import com.ensta.rentmanager_maven.service.VehicleService;

@Controller
@RequestMapping("/cars")
public class VehicleController {
	
	@Autowired
	private VehicleService vehicleService;
	
	
	@GetMapping
	public String getAll(Model model, RedirectAttributes redirectAttributes) {
		try {
			List<Vehicle> vehicles = vehicleService.findAll();
			model.addAttribute("listVehicles", vehicles);
			
		} catch (ServiceException e) {
			redirectAttributes.addAttribute("errorMessage", e.getMessage());
			return "redirect:/cars/error";
		}
		return "/vehicles/list";
	}
	
	@GetMapping("/create")
	public String create(Model model)
	{
		
		return "/vehicles/create";
	}
	
	@PostMapping("/create")
	public String post(@ModelAttribute("vehicle") Vehicle vehicle, RedirectAttributes redirectAttributes)
	{
		try	{
			vehicleService.create(vehicle);
		} catch (ServiceException e) {
			redirectAttributes.addAttribute("errorMessage", e.getMessage());
			return "redirect:/cars/error";
		}		
		return "redirect:/cars";
	}
	
	@PostMapping("/delete")
	public String delete(Model model, int id, RedirectAttributes redirectAttributes)
	{
		try {
			vehicleService.delete(id);
		} catch (ServiceException e) {
			redirectAttributes.addAttribute("errorMessage", e.getMessage());
			return "redirect:/cars/error";
		}			
		return "redirect:/cars";
	}
	
	@GetMapping("/error")
	public String showError(Model model, String errorMessage)
	{
		model.addAttribute("errorMessage", errorMessage);
		return "/error";
	}

}
