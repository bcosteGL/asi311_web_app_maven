package com.ensta.rentmanager_maven.ui.cli;

import java.util.List;
import java.util.ListIterator;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.ensta.rentmanager_maven.configuration.AppConfiguration;
import com.ensta.rentmanager_maven.exception.ServiceException;
import com.ensta.rentmanager_maven.model.Vehicle;
import com.ensta.rentmanager_maven.service.ClientService;
import com.ensta.rentmanager_maven.service.VehicleService;
import com.ensta.rentmanager_maven.utils.IOUtils;

public class VehicleInterface {
	
	
	private VehicleService vehicleService;
	
	public VehicleInterface()
	{
		ApplicationContext context = new AnnotationConfigApplicationContext(AppConfiguration.class);
		this.vehicleService = context.getBean(VehicleService.class);
	}
	
	public void create()
	{
		String constructor = IOUtils.readString("Création du vehicule :\n --> Entrez le constructeur : ", true);
		String model = IOUtils.readString(" --> Entrez le modèle : ", true);
		int size = IOUtils.readInt(" --> Entrez le nombre de place(s) : ");
		
		Vehicle vehicle = new Vehicle(constructor, model, size);
		
		try {
			long id = vehicleService.create(vehicle);
			IOUtils.print("Vehicule créé, id numéro " + id);
		} catch(ServiceException e) {
			IOUtils.print("Erreur lors de la création du vehicule");
		}
	}
	
	public void delete(long id)
	{
		try {
			vehicleService.delete(id);
		} catch (ServiceException e) {
			IOUtils.print("Erreur lors de la suppression de la réservation");
		}
	}
	
	public void listAll()
	{
		try {
			
			Vehicle vehicle = new Vehicle();
			List<Vehicle> listVehicle = vehicleService.findAll();
			ListIterator<Vehicle> it = listVehicle.listIterator();
			while(it.hasNext()) {
				vehicle = it.next();
				IOUtils.print(vehicle.toString());
			}
			
		} catch (ServiceException e) {
			IOUtils.print("Erreur lors du chargement des vehicules");
		}
	}
}
