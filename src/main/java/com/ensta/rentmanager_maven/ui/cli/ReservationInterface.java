package com.ensta.rentmanager_maven.ui.cli;

import java.time.LocalDate;
import java.util.List;
import java.util.ListIterator;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.ensta.rentmanager_maven.configuration.AppConfiguration;
import com.ensta.rentmanager_maven.exception.ServiceException;
import com.ensta.rentmanager_maven.model.Reservation;
import com.ensta.rentmanager_maven.service.ClientService;
import com.ensta.rentmanager_maven.service.ReservationService;
import com.ensta.rentmanager_maven.utils.IOUtils;

public class ReservationInterface {
	
	
	private ReservationService reservationService;
	
	public ReservationInterface()
	{
		ApplicationContext context = new AnnotationConfigApplicationContext(AppConfiguration.class);
		this.reservationService = context.getBean(ReservationService.class);
	}
	
	public void create()
	{
		int clientId = IOUtils.readInt("Création de la reservation :\n --> Entrez un id client : ");
		int vehicleId = IOUtils.readInt(" --> Entrez un id vehicule : ");
		LocalDate startDate = IOUtils.readDate(" --> Entrez la date de début au format dd/MM/yyyy : ", true);
		LocalDate endDate = IOUtils.readDate(" --> Entrez la date de fin au format dd/MM/yyyy : ", true);
		
		Reservation reservation = new Reservation(clientId, vehicleId, startDate, endDate);
		
		try {
			long id = reservationService.create(reservation);
			IOUtils.print("Reservation créé, id numéro " + id);
		} catch(ServiceException e) {
			IOUtils.print("Erreur lors de la création de la reservation");
		}
	}
	
	public void delete(long id)
	{
		try {
			reservationService.delete(id);
		} catch (ServiceException e) {
			IOUtils.print("Erreur lors de la suppression de la réservation");
		}
	}
	
	public void listAll()
	{
		try {
			
			Reservation reservation = new Reservation();
			List<Reservation> listReservation = reservationService.findAll();
			ListIterator<Reservation> it = listReservation.listIterator();
			while(it.hasNext()) {
				reservation = it.next();
				IOUtils.print(reservation.toString());
			}
			
		} catch (ServiceException e) {
			IOUtils.print("Erreur lors du chargement des reservations");
		}
	}
	public void listByClient(long idClient)
	{
		try {
			
			Reservation reservation = new Reservation();
			List<Reservation> listReservation = reservationService.findByIdCli(idClient);
			ListIterator<Reservation> it = listReservation.listIterator();
			while(it.hasNext()) {
				reservation = it.next();
				IOUtils.print(reservation.toString());
			}
			
		} catch (ServiceException e) {
			IOUtils.print("Erreur lors du chargement des reservations");
		}
	}
	public void listByVehicle(long idVehicle)
	{
		try {
			
			Reservation reservation = new Reservation();
			List<Reservation> listReservation = reservationService.findByIdCli(idVehicle);
			ListIterator<Reservation> it = listReservation.listIterator();
			while(it.hasNext()) {
				reservation = it.next();
				IOUtils.print(reservation.toString());
			}
			
		} catch (ServiceException e) {
			IOUtils.print("Erreur lors du chargement des reservations");
		}
	}
}