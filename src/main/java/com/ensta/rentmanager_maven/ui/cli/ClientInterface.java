package com.ensta.rentmanager_maven.ui.cli;
import java.time.LocalDate;
import java.util.List;
import java.util.ListIterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import com.ensta.rentmanager_maven.configuration.AppConfiguration;
import com.ensta.rentmanager_maven.exception.ServiceException;
import com.ensta.rentmanager_maven.model.Client;
import com.ensta.rentmanager_maven.service.ClientService;
import com.ensta.rentmanager_maven.utils.*;

public class ClientInterface {
	
	private ClientService clientService;
	
	public ClientInterface()
	{
		ApplicationContext context = new AnnotationConfigApplicationContext(AppConfiguration.class);
		this.clientService = context.getBean(ClientService.class);
	}
	
	public void create()
	{
		String name = IOUtils.readString("Création du client :\n --> Entrez un nom : ", true);
		IOUtils.print(name);
		String firstName = IOUtils.readString(" --> Entrez un prénom : ", true);
		LocalDate birthDate = IOUtils.readDate(" --> Entrez une date de naissance au format dd/MM/yyyy : ", true);
		Boolean loop = true;
		String email = "";
		while(loop) {
			email = IOUtils.readString(" --> Entrez un e adresse email : ", true);
			if (email.contains("@")) {
				loop = false;
			} else {
				IOUtils.print("Adresse email non valide");
			}
		}
		
		Client client = new Client(name, firstName, email, birthDate);
		
		try {
			long id = clientService.create(client);
			IOUtils.print("Client créé, id numéro " + id);
		} catch(ServiceException e) {
			IOUtils.print("Erreur lors de la création du client");
		}
	}
	
	public void delete(long id)
	{
		try {
			clientService.delete(id);
		} catch (ServiceException e) {
			IOUtils.print("Erreur lors de la suppression de la réservation");
		}
	}
	
	public void listAll()
	{
		try {
			
			Client client = new Client();
			List<Client> listClient = clientService.findAll();
			ListIterator<Client> it = listClient.listIterator();
			while(it.hasNext()) {
				client = it.next();
				IOUtils.print(client.toString());
			}
			
		} catch (ServiceException e) {
			IOUtils.print("Erreur lors du chargement des clients");
		}
	}
}
