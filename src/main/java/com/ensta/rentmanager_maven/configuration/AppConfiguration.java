package com.ensta.rentmanager_maven.configuration;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.h2.jdbcx.JdbcDataSource;
import org.hibernate.Session;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;

import com.ensta.rentmanager_maven.persistence.ConnectionManager;

@Configuration
@ComponentScan({"com.ensta.rentmanager_maven.service","com.ensta.rentmanager_maven.dao"})
public class AppConfiguration {
   @Bean
   public LocalSessionFactoryBean sessionFactory() {
       LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
       sessionFactory.setDataSource(dataSource());
       sessionFactory.setPackagesToScan("com.ensta.rentmanager_maven.model");
       return sessionFactory;
   }

   @Bean
   public DataSource dataSource() {
       JdbcDataSource dataSource = new JdbcDataSource();
       dataSource.setURL("jdbc:h2:~/h2.database/RentManagerDatabase");
       dataSource.setUser("");
       dataSource.setPassword("");

       return dataSource;
   }

   @Bean
   public Session getSession(LocalSessionFactoryBean localSessionFactoryBean) {
       return localSessionFactoryBean.getObject().openSession();
   }
   
   @Bean
	public Connection jdbcConnection() throws SQLException{
		return new ConnectionManager().getConnection();
	}
}