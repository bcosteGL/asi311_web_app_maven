package com.ensta.rentmanager_maven.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ensta.rentmanager_maven.dao.ClientDao;
import com.ensta.rentmanager_maven.dao.ReservationDao;
import com.ensta.rentmanager_maven.exception.DaoException;
import com.ensta.rentmanager_maven.exception.ServiceException;
import com.ensta.rentmanager_maven.model.Reservation;

@Service
public class ReservationService {
	
	private ReservationDao reservationDao;
	   
	@Autowired
	public ReservationService(ReservationDao reservationDao)
	{
		this.reservationDao = reservationDao;
	}
	  		
		
	public long create(Reservation reservation) throws ServiceException {
		long i = -1;
		if (reservation.getEndDate().isBefore(reservation.getStartDate())) {
			throw new ServiceException("Fin de réservation doit être après le début");
		}
		
		try {
			i = reservationDao.create(reservation);
		}  catch (DaoException e) {
			e.printStackTrace();		
		} 
		
		return i;
	}

	public Reservation findById(long id) throws ServiceException {
		try {
			return reservationDao.findResaById(id);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return null;
	}
	
	public List<Reservation> findByIdCli(long id) throws ServiceException {
		List<Reservation> reservations = new ArrayList<Reservation>();		
		try {
			reservations = reservationDao.findResaByClientId(id);
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return reservations;
	}
	
	public List<Reservation> findByIdVeh(long id) throws ServiceException {
		List<Reservation> reservations = new ArrayList<Reservation>();		
		try {
			reservations = reservationDao.findResaByVehicleId(id);
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return reservations;
	}

	public List<Reservation> findAll() throws ServiceException {
		List<Reservation> reservations = new ArrayList<Reservation>();		
		try {
			reservations = reservationDao.findAll();
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return reservations;
	}
	
	public long delete(long id) throws ServiceException
	{
		long i = -1;
		try {
			Reservation reservation = reservationDao.findResaById(id);
			i = reservationDao.delete(reservation);
		}  catch (DaoException e) {
			e.printStackTrace();		
		} 
		
		return i;
	}
}
