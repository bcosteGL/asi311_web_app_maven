package com.ensta.rentmanager_maven.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ensta.rentmanager_maven.dao.ClientDao;
import com.ensta.rentmanager_maven.dao.VehicleDao;
import com.ensta.rentmanager_maven.exception.DaoException;
import com.ensta.rentmanager_maven.exception.ServiceException;
import com.ensta.rentmanager_maven.model.Vehicle;

@Service
public class VehicleService {

   private VehicleDao vehicleDao;
   
	@Autowired
	public VehicleService(VehicleDao vehicleDao)
	{
		this.vehicleDao = vehicleDao;
	}
   
	public long create(Vehicle vehicle) throws ServiceException {
		long i = -1;
		if (vehicle.getConstructor()==null) {
			throw new ServiceException("Please fill the constructor");
		}
		else {
			if (vehicle.getSize()<1) {
				throw new ServiceException("Please fill the size");
			}
			try {
				i = vehicleDao.create(vehicle);
			}  catch (DaoException e) {
				e.printStackTrace();
				throw new ServiceException("Service Exception : Couldn't create this vehicle");
			} 
		}
		return i;
	}

	public Optional<Vehicle> findById(long id) throws ServiceException {
		try {
			return vehicleDao.findById(id);
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServiceException("Service Exception : Couldn't find the vehicle");
		} 
	}

	public List<Vehicle> findAll() throws ServiceException {
		List<Vehicle> vehicles = new ArrayList<Vehicle>();		
		try {
			vehicles = vehicleDao.findAll();
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServiceException("Service Exception : Couldn't find the vehicles");
		}
		return vehicles;
	}

	public void delete(long id) throws ServiceException
	{
		try {
			Optional<Vehicle> vehicleOpt = vehicleDao.findById(id);
			Vehicle vehicle = null;
			if (vehicleOpt.isPresent()) {
				vehicle = vehicleOpt.get();
				vehicleDao.delete(vehicle);
			}
		}  catch (DaoException e) {
			e.printStackTrace();
			throw new ServiceException("Service Exception : Couldn't delete this vehicle");
		} 
	}
	
	public int count() throws ServiceException
	{
		int i = -1;
		try {
			i = vehicleDao.count();
		}  catch (DaoException e) {
			e.printStackTrace();
			throw new ServiceException("Service Exception : Couldn't count the number of vehicles");
		} 
		
		return i;
	}
}
