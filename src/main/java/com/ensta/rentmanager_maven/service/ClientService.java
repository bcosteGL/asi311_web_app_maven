package com.ensta.rentmanager_maven.service;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.ensta.rentmanager_maven.dao.ClientDao;
import com.ensta.rentmanager_maven.exception.DaoException;
import com.ensta.rentmanager_maven.exception.ServiceException;
import com.ensta.rentmanager_maven.model.Client;

@Service
public class ClientService {


   private ClientDao clientDao;
   
   private static final String emailRegex = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
   
   @Autowired
   public ClientService(ClientDao clientDao)
   {
	   this.clientDao = clientDao;
   }
	
   public static boolean isValid(String email) 
   { 
                             
       Pattern pat = Pattern.compile(emailRegex); 
       if (email == null) 
           return false; 
       return pat.matcher(email).matches(); 
   } 
   
	public long create(Client client) throws ServiceException {
		long i = -1;
		if (!isValid(client.getEmail())) {
			throw new ServiceException("Email invalide");
		}
		if ( (Period.between(client.getBirthDate(), LocalDate.now()).getYears()) <= 18 )
			throw new ServiceException("Must be over 18");

		if (client.getFirstName()==null) {
			throw new ServiceException("First name empty");
		}
		else {
			if (client.getName()==null) {
				throw new ServiceException("Name empty");
			}
			else {
				try {
					client.setName(client.getName().toUpperCase());
					i = clientDao.create(client);
				}  catch (DaoException e) {
					e.printStackTrace();
					throw new ServiceException("Service Exception : Couldn't create the client");

				} 
			}
		}	
		return i;
	}

	public Optional<Client> findById(long id) throws ServiceException {
		try {
			return clientDao.findById(id);
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServiceException("Service Exception : Couldn't find the specified client");

		} 
	}

	public List<Client> findAll() throws ServiceException {
		List<Client> clients = new ArrayList<Client>();		
		try {
			clients = clientDao.findAll();
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServiceException("Service Exception : Couldn't find all the clients");

		}
		return clients;
	}
	
	public void delete(long id) throws ServiceException
	{
		Client client = null;
		try {
			Optional<Client> clientOpt = clientDao.findById(id);
			if (clientOpt.isPresent()) {
				client = clientOpt.get();
			}
			clientDao.delete(client);
		}  catch (DaoException e) {
			e.printStackTrace();
			throw new ServiceException("Service Exception : Couldn't delete the client");

		} 
	}
	public int count() throws ServiceException
	{
		int i = -1;
		try {
			i = clientDao.count();
		}  catch (DaoException e) {
			e.printStackTrace();	
			throw new ServiceException("Service Exception : Couldn't count the number of clients");
		} 
		
		return i;
	}
}
