package com.ensta.rentmanager_maven.persistence;

import java.sql.Connection;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import java.sql.SQLException;

import org.h2.jdbcx.JdbcDataSource;

@Component
@Scope("singleton")
public class ConnectionManager {
	private static final String DB_CONNECTION = "jdbc:h2:~/h2.database/RentManagerDatabase";
	private static final String DB_USER = "";
	private static final String DB_PASSWORD = "";

	private static JdbcDataSource datasource = null;

	private static void init() {
		if (datasource == null) {
			datasource = new JdbcDataSource();
			datasource.setURL(DB_CONNECTION);
			datasource.setUser(DB_USER);
			datasource.setPassword(DB_PASSWORD);
		}
	}

	public Connection getConnection() throws SQLException {
		init();
		return datasource.getConnection();
	}
	
	static public Connection getConnectionStatic() throws SQLException {
		init();
		return datasource.getConnection();
	}
	
	public boolean test()
	{
		Connection connection = null;
		try {
			connection = getConnection();
		} catch (SQLException e) {
			return false;
		}
		return (connection != null);
	}

}